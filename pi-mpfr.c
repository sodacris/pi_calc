#include "stdio.h"
#include <math.h>
#include <mpfr.h>
#include <time.h>

#define PREC 4000

int main(void) {

  mpfr_t mid, arr[4], value, pi;
  mpfr_prec_t prec = PREC;
  mpfr_rnd_t rnd = MPFR_RNDA;

  mpfr_init2(mid, prec);
  mpfr_init2(value, prec);
  mpfr_init2(pi, prec);

  for (int i = 0; i < 4; i++) {
    mpfr_init2(arr[i], prec);
  }
  for (int i = 0; i < 4; i++) {
    mpfr_set_d(arr[i], 0, prec);
  }

  mpfr_set_d(mid, 0, rnd);
  mpfr_set_d(value, 0, rnd);
  mpfr_set_d(pi, 0, rnd);

  // 时间
  time_t t_begin, t_end;
  t_begin = time(0);

  // pi=(2*sqrt(2)/9801) * val)^(-1)
  mpfr_sqrt_ui(arr[3], 2UL, rnd);
  mpfr_mul_ui(arr[3], arr[3], 2, rnd);
  mpfr_div_ui(arr[3], arr[3], 9801UL, rnd);

  int li = 30;

  printf("项数: ");
  scanf("%d", &li);

  unsigned long k = 0;
  while (1) {
    // val+=(4k)!(1103+26390k)/((k!)^4)(396^(4k))
    mpfr_set_ui(mid, 1103 + 26390 * k, rnd);
    // (4k)!
    mpfr_fac_ui(arr[0], 4 * k, rnd);
    mpfr_mul(mid, mid, arr[0], rnd);
    // k!^4
    mpfr_fac_ui(arr[1], k, rnd);
    mpfr_pow_ui(arr[1], arr[1], 4, rnd);
    // 396^4k
    mpfr_ui_pow_ui(arr[2], 396UL, 4 * k, rnd);
    // k!*396^4k
    mpfr_mul(arr[1], arr[1], arr[2], rnd);

    mpfr_div(mid, mid, arr[1], rnd);
    mpfr_add(value, value, mid, rnd);
    k += 1;

    mpfr_mul(pi, value, arr[3], rnd);
    mpfr_ui_div(pi, 1UL, pi, rnd);

    // printf("%lu\n", k);
    if (k >= li) {
      break;
    }
  }

  t_end = time(0);
  double seconds = difftime(t_end, t_begin);
  FILE *f = fopen("out.txt", "w");

  mpfr_out_str(f, 10, PREC, pi, rnd);
  fclose(f);

  char pi_s[PREC + 10] = {'\0'}, out[PREC + 10] = {'\0'};
  FILE *f_pi = fopen("pi.txt", "r");
  FILE *f_out = fopen("out.txt", "r");
  fread(pi_s, sizeof(char), PREC + 10, f_pi);
  fread(out, sizeof(char), PREC + 10, f_out);
  int out_prec = 0;
  for (int i = 0; i < PREC + 10; i++) {
    if (pi_s[i] != out[i]) {
      out_prec = i;
      break;
    }
  }
  //   printf("%s\n", out);

  out[out_prec] = '\0';
  printf("项：%lu 时间：%.fs 精度：%d\n值：%s\n", k, seconds, out_prec - 2,
         out);
  return 0;
}