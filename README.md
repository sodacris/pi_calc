# Pi值计算

使用 Srinivasa Ramanujan公式计算pi值：

![formula](formula.png)

详见[Wikipedia](https://en.wikipedia.org/wiki/Approximations_of_%CF%80).

使用[`mpfr`库](https://www.mpfr.org/)

运行：

```bash
cc -o pi-mpfr.out pi-mpfr.c -lgmp -lmpfr && ./pi-mpfr.out
```

效果：
![result](result.png)

`pi.txt`为取自[WolframAlpha](https://www.wolframalpha.com/input/?i=pi)的数据，精度为`517`位。